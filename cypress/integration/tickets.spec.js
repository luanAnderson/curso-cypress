describe("Tickets",() => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));

    it("fills all the text input field",() =>{
        const firstName = "Luan";
        const lastName= "Santos";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("luanlindo@gmail.com");
        cy.get("#requests").type("Carnívoro");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("select option two tckets", () =>{
        cy.get("#ticket-quantity").select("2");

    });

    it("select radio'vip' ticket type",() => {
        cy.get('#vip').check()

    });

    it("select 'social media' checkbox",() =>{
        cy.get("#social-media").check()

    });
    
    it("selects 'friend', and 'publication', the uncheck 'friend'",() =>{
        cy.get("#friend").check()
        cy.get("#publication").check()
        cy.get("#friend").uncheck()


    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");

    });
    it("alerts on invalid email", () => {
        cy.get("#email")
            .as("email")
            .type("luanlindo-gmail.com");
        
        cy.get("#email.invalid").should("exist");

        cy.get("@email")
            .clear()
            .type("luanlindo@teste.com");
        
        cy.get("#email.invalid").should("not.exist");
    });    
    it("fills and reset th form", () => {
        const firstName = "Luan";
        const lastName= "Santos";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("luanlindo@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get('#vip').check()
        cy.get("#friend").check()
        cy.get("#requests").type("automation");
        
        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
        .as('submitButton')
        .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");

    });

    it("fills sandatory fields using suport command",  () => {
         const customer= { 
            firstName : "Pedro",
            lastName : "Silva",
            email : "pedrosilva@exempl.com"
    };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
        .as('submitButton')
        .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");

    });
});